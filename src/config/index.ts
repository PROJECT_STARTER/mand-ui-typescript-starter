export default {
  http: {
    prefix: {
      product:
        process.env.NODE_ENV === "production"
          ? "https://emas.chinarecrm.com.cn/"
          : "/api",
      test:
        process.env.NODE_ENV === "production"
          ? "https://emas.kerbores.com/"
          : "/api",
    },
    timeout: 60 * 1000,
  },
  app: {
    name: "种果乐",
    copyright: "Copyright ©️ chinare.com.cn ",
    ossPrefix: {
      product:
        process.env.NODE_ENV === "production"
          ? "https://emas.chinarecrm.com.cn/download/"
          : "/api/download/",
      test:
        process.env.NODE_ENV === "production"
          ? "https://emas.kerbores.com/download/"
          : "/api/download/",
    },
  },
};
