/* eslint-disable @typescript-eslint/no-explicit-any */
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./plugins/plugin";

import "mand-mobile/components/_style/global.styl";
import "reset.css";
import "normalize.css";
import "font-awesome/css/font-awesome.css";
import FastClick from "fastclick";

import { ApiPlugin } from "@/api";
import GlobalMixin from "@/mixins/GlobalMixin";
Vue.config.productionTip = false;

Vue.use(ApiPlugin);
Vue.mixin(GlobalMixin);

if ("addEventListener" in document && "ontouchstart" in window) {
  FastClick.prototype.focus = function (targetElement: any) {
    targetElement.focus();
  };
  document.addEventListener(
    "DOMContentLoaded",
    function () {
      FastClick.attach(document.body);
    },
    false
  );
}
router.beforeEach((to, _from, next) => {
  store.commit("setGlobal", to.meta);
  next();
});

if (process.env.NODE_ENV === "production") {
  if (window.plus) {
    new Vue({
      router,
      store,
      render: (h) => h(App),
    }).$mount("#app");
  } else {
    document.addEventListener(
      "plusready",
      () => {
        new Vue({
          router,
          store,
          render: (h) => h(App),
        }).$mount("#app");
      },
      false
    );
  }
} else {
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount("#app");
}
