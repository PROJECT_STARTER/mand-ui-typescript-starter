/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 *取 x y间的随机整数
 *
 * @export
 * @param {number} x
 * @param {number} [y]
 * @returns
 */
export function random(x: number, y?: number) {
  if (y && y > x) {
    return Math.ceil(Math.random() * (y - x + 1) + x);
  } else if (y) {
    return Math.ceil(Math.random() * (x - y + 1) + y);
  } else {
    return Math.ceil(Math.random() * x + 1);
  }
}

/**
 *格式化银行卡号
 *
 * @export
 * @param {string} cardNumber 银行卡号
 * @returns 格式化卡号
 */
export function formatBankCard(cardNumber: string) {
  if (!isNaN(Number(cardNumber.replace(/[ ]/g, ""))))
    return cardNumber.replace(/\s/g, "").replace(/(\d{4})(?=\d)/g, "$1 ");
  return "";
}

/**
 *包含
 *
 * @export
 * @param {*} value
 * @param {Array<any>} validList
 * @returns
 */
export function oneOf(value: any, validList: Array<any>) {
  for (let i = 0; i < validList.length; i++) {
    if (value === validList[i]) {
      return true;
    }
  }
  return false;
}

/**
 *
 *
 * @export
 * @returns
 */
export function randomString() {
  return Math.random().toString(36).slice(-8);
}
