import { Vue, Component } from "vue-property-decorator";

import config from "@/config";
declare module "vue/types/vue" {
  interface Vue {
    isIPhoneX: boolean;
    native: boolean;
    appName: string;
  }
}

@Component
export default class GlobalMixin extends Vue {
  get appName() {
    return config.app.name;
  }
  /**
   *刷新页面
   *
   * @memberof GloaleMixin
   */
  refresh() {
    window.location.reload();
  }

  get isIPhoneX() {
    return this.$plus.device.model === "iPhoneX";
  }

  get native() {
    return !!window.plus;
  }
}
