import { PluginObject } from "vue";

import { default as emasApi, EmasApi } from "./emas/mods";

export interface Api {
  emasApi: EmasApi;
}

export const ApiPlugin: PluginObject<Api> = {
  install: (Vue) => {
    Object.defineProperties(Vue.prototype, {
      $api: {
        get() {
          return {
            emasApi,
          };
        },
      },
    });
  },
};
