import { PluginObject } from "vue";
import plus from "./plus";
import { Plus } from "./plus/index";

export const H5Plugin: PluginObject<Plus> = {
  install: (Vue) => {
    Object.defineProperties(Vue.prototype, {
      $plus: {
        get() {
          return window.plus || plus;
        },
      },
    });
  },
};
