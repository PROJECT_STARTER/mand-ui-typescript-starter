import { AxiosInstance } from "axios";
import { Plus } from "./plus/index";
import { Api } from "@/api/index";
declare global {
  interface Window {
    axios: AxiosInstance;
    plus: Plus;
    $api: Api;
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $axios: AxiosInstance;
    $plus: Plus;
    $api: Api;
  }
  interface VueConstructor {
    $axios: AxiosInstance;
    $plus: Plus;
    $api: Api;
  }
}
