/* eslint-disable @typescript-eslint/no-explicit-any */
import { PluginObject } from "vue";
import axios from "axios";
import settings from "@/config/index";
import store from "@/store";
import { Toast } from "mand-mobile";
import { AxiosInstance } from "axios";

export function defaultSuccess(data: any) {
  console.log(data);
}
export function defaultError(error: string) {
  Toast({
    content: error,
    icon: "wrong",
    position: "top",
    hasMask: true,
  });
}
export function msgAndLogout(msg: string) {
  console.log(msg);
}

const config = {
  baseURL:
    store.state.operateEnvironment === "product"
      ? settings.http.prefix.product
      : settings.http.prefix.test,
  timeout: settings.http.timeout,
};

const _axios = axios.create(config);

_axios.interceptors.request.use(
  (cfg) => {
    const token = store && store.getters.token;
    if (token) {
      cfg.headers.Authorization = token;
    }
    return cfg;
  },
  (err) => {
    console.warn(err);
    return Promise.reject("请求配置失败!");
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  (response) => {
    console.log(response);
    if (response.data.operationState == "SUCCESS") {
      //数据成功
      return Promise.resolve(response.data.data);
    } else {
      //数据失败直接reject
      return Promise.reject(response.data.errors[0]);
    }
  },
  (error) => {
    console.log(error);
    switch (error.response.status) {
      case 403 | 401:
        msgAndLogout("您没有权限操作此功能,请联系平台管理员");
        break;
      case 404:
        return Promise.reject("不存在的接口");
      case 500:
        return Promise.reject(error.response.data.errors[0]);
      default:
        return Promise.reject(error.response.data.msg[0]);
    }
  }
);

const Plugin: PluginObject<AxiosInstance> = {
  install: (Vue) => {
    Vue.$axios = _axios;
    window.axios = _axios;
    Object.defineProperties(Vue.prototype, {
      $axios: {
        get() {
          return _axios;
        },
      },
    });
  },
};

export { _axios as http, Plugin as axiosPlugin };
