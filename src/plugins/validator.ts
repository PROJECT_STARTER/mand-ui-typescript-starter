import Vue from "vue";
import VeeValidate, { Validator } from "vee-validate";
import zh_CN from "vee-validate/dist/locale/zh_CN";
Validator.extend("phone", {
  getMessage: () => `手机号码格式不正确`,
  validate: (value: string) => /^1[34578][0-9]{9}$/.test(value),
});

Vue.use(VeeValidate, {
  dictionary: {
    zh_CN: zh_CN,
  },
});

Validator.localize("zh_CN");
