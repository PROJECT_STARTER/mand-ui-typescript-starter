import Vue from "vue";
import { axiosPlugin } from "./axios";
import { H5Plugin } from "./h5";

import "./validator";
import "./vconsole";
Vue.use(axiosPlugin);
Vue.use(H5Plugin);
