/* eslint-disable @typescript-eslint/no-explicit-any */
import { ErrorCallBack } from "./exception";
export class Speech {
  startRecognize(
    options: SpeechRecognizeOptions,
    successCB?: (result: string) => any,
    errorCB?: ErrorCallBack
  ): void {
    console.log(options, successCB, errorCB);
  }
  stopRecognize(): void {
    console.log("Speech stopRecognize");
  }
  addEventListener(
    event:
      | "start"
      | "volumeChange"
      | "recognizing"
      | "recognition"
      | "end"
      | "error",
    listener: (event: any) => void,
    capture?: boolean
  ): void {
    console.log("Speech addEventListener", event, listener, capture);
  }
}

export class SpeechRecognizeOptions {
  constructor(
    public lang: "zh-cn" | "en-us" | "zh-cantonese" | "zh-henanese" = "zh-cn",
    public punctuation: boolean = true,
    public nbest: number = -1,
    public timeout: number = 10000,
    public userInterface: boolean = true,
    public engine: "iFly" | "baidu" = "iFly"
  ) {}
}

export default new Speech();
