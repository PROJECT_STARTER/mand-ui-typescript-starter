import { random } from "../../core/utils";
export class Screen {
  setBrightness(brightness: number) {
    this._brightness = brightness;
  }

  getBrightness(): number {
    return this._brightness;
  }
  lockOrientation(
    orientation:
      | "portrait-primary"
      | "portrait-secondary"
      | "landscape-primary"
      | "landscape-secondary"
      | "portrait"
      | "landscape"
  ) {
    console.log("screen lockOrientation", orientation);
    this._orientation = orientation;
  }
  unlockOrientation() {
    console.log("screen unlockOrientation");
    this._orientation = "portrait-primary";
  }

  _orientation = "portrait-primary";
  _brightness = 0.5;
  constructor(
    readonly dpiX: number,
    readonly dpiY: number,
    readonly height: number,
    readonly width: number,
    readonly resolutionHeight: number,
    readonly resolutionWidth: number,
    readonly scale: number
  ) {}
}

export default new Screen(
  random(100),
  random(100),
  random(100),
  random(100),
  random(100),
  random(100),
  1
);
