import accelerometer from "./accelerometer";
import barcode from "./barcode";
import speech from "./speech";
import navigator from "./navigator";
import audio from "./audio";
import device from "./device";
import gallery from "./gallery";
import webview from "./webview";
import screen from "./screen";
import geolocation from "./geolocation";
import { Accelerometer } from "./accelerometer";
import { BarcodeApi } from "./barcode";
import { Speech } from "./speech";
import { Navigator } from "./navigator";
import { Audio } from "./audio";
import { Device } from "./device";
import { Gallery } from "./gallery";
import { WebviewApi } from "./webview";
import { Screen } from "./screen";
import { NativeObj } from "./nativeObj";
import { Geolocation } from "./geolocation";
export class Plus {
  constructor(
    public accelerometer: Accelerometer,
    public barcode: BarcodeApi,
    public speech: Speech,
    public navigator: Navigator,
    public audio: Audio,
    public device: Device,
    public gallery: Gallery,
    public webview: WebviewApi,
    public screen: Screen,
    public nativeObj: NativeObj,
    public geolocation: Geolocation
  ) {}
}
export default {
  accelerometer,
  barcode,
  speech,
  navigator,
  audio,
  device,
  gallery,
  webview,
  screen,
  geolocation,
} as Plus;
