import { Exception } from "./exception";
export class Geolocation {
  getCurrentPosition(
    successCB: (position: Position) => void,
    errorCB?: (error: Exception) => void,
    option?: PositionOptions
  ) {
    console.log(successCB, errorCB, option);
  }
  watchPosition(
    successCB: (position: Position) => void,
    errorCB?: (error: Exception) => void,
    option?: PositionOptions
  ): number {
    return window.setInterval(
      () => {
        this.getCurrentPosition(successCB, errorCB, option);
      },
      option ? option.maximumAge : 5000
    );
  }
  clearWatch(watchId: number) {
    window.clearInterval(watchId);
  }
}

export class GeolocationError extends Exception {
  readonly PERMISSION_DENIED: number = 1;
  readonly POSITION_UNAVAILABLE: number = 2;
  readonly TIMEOUT: number = 3;
  readonly UNKNOWN_ERROR: number = 4;
}

export class Coordinates {
  latitude = 1;
  longitude = 1;
  altitude = 1;
  accuracy = 1;
  altitudeAccuracy = 1;
  heading = 0;
  speed = 1;
}

export class Position {
  coordsType?: "gps" | "gcj02" | "bd09" | "bd09ll";
  timestamp = 0;
  address?: Address;
  addresses?: string;
  coords: Coordinates = new Coordinates();
}

export class Address {
  country?: string;
  province?: string;
  city?: string;
  district?: string;
  street?: string;
  streetNum?: string;
  poiName?: string;
  postalCode?: string;
  cityCode?: string;
}

export class PositionOptions {
  enableHighAccuracy?: boolean;
  timeout?: number;
  maximumAge?: number;
  provider?: "system" | "baidu" | "amap";
  coordsType?: "gps" | "gcj02" | "bd09" | "bd09ll";
  geocode?: boolean;
}

export default new Geolocation();
