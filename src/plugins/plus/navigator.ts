export class Navigator {
  setStatusBarBackground(color: string) {
    console.log(color);
  }
  setStatusBarStyle(style: "light" | "dark") {
    console.log(style);
  }
}

export default new Navigator();
