export class Exception {
  constructor(public code: number, public message: string) {}
}

export interface ErrorCallBack {
  (error: Exception): void;
}
const errorCallback: ErrorCallBack = function (error: Exception) {
  console.log(error.code, error.message);
};
export default errorCallback;
