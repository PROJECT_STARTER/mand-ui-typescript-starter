/* eslint-disable @typescript-eslint/no-explicit-any */
import { ErrorCallBack, Exception } from "./exception";
import { randomString } from "@/core/utils";

/* eslint-disable @typescript-eslint/no-namespace */
export namespace plus.device {
  export class DeviceInfo {
    constructor(
      readonly imei: string,
      readonly imsi: Array<string>,
      readonly uuid: string
    ) {}
  }
}
export class Device {
  beep(times: number) {
    console.log("device beep", times);
  }
  dial(number: string, confirm?: boolean) {
    console.log("device dail", number, confirm);
  }

  getInfo(options: {
    success: (event: plus.device.DeviceInfo) => void;
    fail: ErrorCallBack;
    complete: (event: Exception | any) => void;
  }) {
    options.success(
      new plus.device.DeviceInfo(
        randomString(),
        [randomString(), randomString()],
        randomString()
      )
    );
  }

  getVolume(): number {
    return this._volume;
  }
  setVolume(volume: number) {
    console.log("device setVolume", volume);
    this._volume = volume;
  }
  isWakelock(): boolean {
    return this._lock;
  }

  setWakelock(lock: boolean) {
    console.log("device setWakelock", lock);
    this._lock = lock;
  }

  vibrate(milliseconds: number) {
    console.log("device vibrate"), milliseconds;
  }

  _lock = false;
  _volume = 0.5;
  constructor(
    readonly imei: string,
    readonly imsi: Array<string>,
    readonly model: string,
    readonly vendor: string,
    readonly uuid: string
  ) {}
}

export default new Device(
  randomString(),
  [randomString(), randomString()],
  randomString(),
  randomString(),
  randomString()
);
