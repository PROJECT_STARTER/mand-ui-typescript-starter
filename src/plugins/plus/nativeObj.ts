export class NativeObj {
  View?: View;
}
export class View {
  close() {
    console.log("view close");
  }
  addEventListener(
    event: "doubleclick" | "click" | "touchstart" | "touchmove" | "touchend",
    listener: (event: TouchEventCallbackEvent) => void,
    capture?: boolean
  ) {
    console.log(event, listener, capture);
  }
  constructor(
    public id: string,
    public styles?: ViewStyles,
    public tags?: Array<ViewDrawTagStyles>
  ) {}
}

export class TouchEventCallbackEvent {
  public clientX?: number;
  public clientY?: number;
  public pageX?: number;
  public pageY?: number;
  public screenX?: number;
  public screenY?: number;
  public currentImageIndex?: number;
  public target?: View;
}
export class ViewStyles {}
export class ViewDrawTagStyles {}
export default new NativeObj();
