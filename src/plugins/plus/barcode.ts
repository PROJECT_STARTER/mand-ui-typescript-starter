/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-namespace */
import { ErrorCallBack } from "./exception";

export namespace plus.barcode {
  export class Barcode {
    cancel() {
      console.log("barcode cancel");
    }
    close() {
      console.log("barcode close");
    }

    setFlash(open: boolean) {
      console.log("barcode setFlash", open);
    }
    setStyle(style: BarcodeStyles) {
      console.log("barcode setStyle", style);
    }
    start(options: BarcodeOptions) {
      console.log("barcode start", options);
    }

    public onmarked?: BarcodeSuccessCallback;
    public onerror?: ErrorCallBack;
    constructor(
      public id: string,
      public filters?: Array<number>,
      public styles?: BarcodeStyles
    ) {}
  }
}
export class BarcodeApi {
  readonly QR: number = 0;
  readonly EAN13: number = 1;
  readonly EAN8: number = 2;
  readonly AZTEC: number = 3;
  readonly DATAMATRIX: number = 4;
  readonly UPCA: number = 5;
  readonly UPCE: number = 6;
  readonly CODABAR: number = 7;
  readonly CODE39: number = 8;
  readonly CODE93: number = 9;
  readonly CODE128: number = 10;
  readonly ITF: number = 11;
  readonly MAXICODE: number = 12;
  readonly PDF417: number = 13;
  readonly RSS14: number = 14;
  readonly RSSEXPANDED: number = 15;
  scan(
    path: string,
    successCB: (type: number, code: string, file?: string) => any,
    errorCB?: () => any,
    filters?: Array<number>
  ): void {
    console.log(path, successCB, errorCB, filters);
  }
  create(
    id: string,
    filters?: Array<number>,
    styles?: BarcodeStyles
  ): plus.barcode.Barcode {
    return new plus.barcode.Barcode(id, filters, styles);
  }
  getBarcodeById(id: string): plus.barcode.Barcode {
    return new plus.barcode.Barcode(id);
  }
}

export interface BarcodeSuccessCallback {
  (type: number, code: string, file?: string): void;
}
export class BarcodeOptions {}
export class BarcodeStyles {
  constructor(
    public top?: string,
    public left?: string,
    public width?: string,
    public height?: string,
    public position?: "static" | "absolute",
    public background?: string,
    public frameColor?: string,
    public scanbarColor?: string
  ) {}
}

export default new BarcodeApi();
