import { View } from "./nativeObj";

export class WebviewObject {
  append(webview: WebviewObject | View) {
    console.log("webview append", webview);
  }
  constructor(public id?: string) {}
}

export class WebviewApi {
  currentWebview(): WebviewObject {
    return new WebviewObject();
  }
}
export default new WebviewApi();
