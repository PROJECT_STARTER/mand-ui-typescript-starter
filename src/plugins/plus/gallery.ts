import { ErrorCallBack } from "./exception";
export class GalleryMultiplePickSuccessCallbackEvent {
  constructor(public files: Array<string>) {}
}
export class GallerySaveEvent {
  constructor(public path: string) {}
}
export class PopPosition {
  public top?: string;
  public left?: string;
  public width?: string;
  public height?: string;
}
export class GalleryOptions {
  public animation?: boolean = true;
  public filename?: string;
  public filter?: "image" | "video" | "none" = "image";
  public maximum?: number = Infinity;
  public multiple?: boolean = false;
  public onmaxed?: () => any;
  public popover?: PopPosition;
  public selected?: Array<string>;
  public system?: boolean = true;
}
export class Gallery {
  pick(
    successCB: (file: string | GalleryMultiplePickSuccessCallbackEvent) => void,
    errorCB?: ErrorCallBack,
    options?: GalleryOptions
  ) {
    console.log(successCB, errorCB, options);
  }
  save(
    path: string,
    successCB: (event: GallerySaveEvent) => void,
    errorCB?: ErrorCallBack
  ) {
    console.log(path, successCB, errorCB);
  }
}

export default new Gallery();
