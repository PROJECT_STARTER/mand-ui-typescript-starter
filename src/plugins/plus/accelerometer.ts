/* eslint-disable @typescript-eslint/no-explicit-any */
import { random } from "../../core/utils";
import { Exception, ErrorCallBack } from "./exception";

/**
 *获取当前设备的加速度信息
 *
 * @export
 * @param {(acceleration: Acceleration) => void} successCB  获取设备加速度信息成功回调函数
 * @param {(error: IException) => void} [errorCB] 获取设备加速度信息失败回调函数
 */
export function getCurrentAcceleration(
  successCB: (acceleration: Acceleration) => any,
  errorCB?: (error: Exception) => void
): any {
  if (random(1, 100) == 1) {
    errorCB &&
      errorCB({
        code: 9999,
        message: "发生了错误",
      });
  } else {
    successCB({
      xAxis: random(1, 100),
      yAxis: random(1, 100),
      zAxis: random(1, 100),
    });
  }
}
/**
 *监听设备加速度变化信息
 *
 * @export
 * @param {(acceleration: Acceleration) => void} successCB 成功回调函数,当获取设备的加速度信息成功时回调，并返回加速度信息
 * @param {(error: IException) => void} [errorCB] 失败回调函数
 * @param {AccelerationOption} [option] 加速度信息参数
 * @returns
 */
export function watchAcceleration(
  successCB: (acceleration: Acceleration) => any,
  errorCB?: (error: Exception) => void,
  option?: AccelerationOption
) {
  return window.setInterval(
    () => {
      getCurrentAcceleration(successCB, errorCB);
    },
    option ? option.frequency : 500
  );
}
/**
 *关闭监听设备加速度信息
 *
 * @export
 * @param {number} watchId 需要取消的加速度监听器标识，调用watchAcceleration方法的返回值。
 */
export function clearWatch(watchId: number) {
  window.clearInterval(watchId);
}

export class Accelerometer {
  getCurrentAcceleration: (
    successCB: (v: Acceleration) => any,
    errorCB?: ErrorCallBack
  ) => void;
  watchAcceleration: (
    successCB: (v: Acceleration) => any,
    errorCB?: ErrorCallBack,
    option?: AccelerationOption
  ) => number;
  clearWatch: (watchId: number) => void;
  constructor() {
    this.getCurrentAcceleration = getCurrentAcceleration;
    this.watchAcceleration = watchAcceleration;
    this.clearWatch = clearWatch;
  }
}

export class Acceleration {
  constructor(
    public xAxis?: number,
    public yAxis?: number,
    public zAxis?: number
  ) {}
}
export class AccelerationOption {
  constructor(public frequency?: number) {}
}
export default new Accelerometer();
