import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    operateEnvironment: "product",
    global: {
      title: "任务列表",
      footer: "task",
    },
    badges: {
      taskCount: 0,
    },
    user: { name: "", token: "" },
    codebooks: [],
  },
  mutations: {
    setGlobal(state, global) {
      state.global = global;
    },
  },
  actions: {},
  getters: {
    token: (state) => {
      return state.user.token;
    },
    codebooks: (state) => {
      return state.codebooks;
    },
  },
  plugins: [createPersistedState()],
});
